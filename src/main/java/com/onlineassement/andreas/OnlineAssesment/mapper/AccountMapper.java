package com.onlineassement.andreas.OnlineAssesment.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.onlineassement.andreas.OnlineAssesment.model.Account;

public class AccountMapper implements RowMapper<Account>{
	
	@Override
    public Account mapRow(ResultSet rs, int i) throws SQLException {
		Account acc = new Account();
		acc.setUser_id(rs.getInt(1));
		acc.setUsername(rs.getString(2));
		acc.setPassword(rs.getString(3));
		acc.setEmail(rs.getString(4));
		
		return acc;
	}

}
