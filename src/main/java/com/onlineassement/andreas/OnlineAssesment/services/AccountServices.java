package com.onlineassement.andreas.OnlineAssesment.services;

import java.sql.Types;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.onlineassement.andreas.OnlineAssesment.mapper.AccountMapper;
import com.onlineassement.andreas.OnlineAssesment.model.Account;

import org.springframework.jdbc.core.JdbcTemplate;

@Service
public class AccountServices {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public Integer insert_account(Account acc){
        String id = "-1";
        String sql = "INSERT INTO accounts(username, password, email) "
                    + "VALUES (?, ?, ?) " + "RETURNING user_id";
        id = jdbcTemplate.queryForObject(sql, String.class, acc.username, acc.password, acc.email);
        return Integer.parseInt(id);
    }
    
    public Account get_account(String email) {
    	Account acc = null;
    	String sql = "Select * from accounts where email= ?";
    	try {
    		acc = jdbcTemplate.queryForObject(sql, new AccountMapper(), email);
    	}catch (Exception e) {
			return null;
		}
    	return acc;
    }
    
    public List<Account> get_all_account() {
    	List<Account> acc = null;
    	String sql = "Select * from accounts";
    	try {
    		acc = jdbcTemplate.query(sql, new AccountMapper(),"");
    	}catch (Exception e) {
			return null;
		}
    	return acc;
    }
    
    public void update_account(Account acc) {
    	String sql = "Update accounts set username = ? , password = ? where email = ?";
    	Object[] params = {acc.username, acc.password, acc.email};
    	int[] types = {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
    	if(jdbcTemplate.update(sql, params, types) == 0)throw new DuplicateKeyException("No Data Updated");
     }
    
    public void delete_account(String email) {
    	String sql = "Delete from accounts where email = ?";
    	Object[] args = new Object[] {email};
    	jdbcTemplate.update(sql, args);
    }
}