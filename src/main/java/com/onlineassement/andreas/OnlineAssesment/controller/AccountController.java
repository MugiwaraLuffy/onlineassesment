package com.onlineassement.andreas.OnlineAssesment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.onlineassement.andreas.OnlineAssesment.model.Account;
import com.onlineassement.andreas.OnlineAssesment.services.AccountServices;
import com.onlineassement.andreas.OnlineAssesment.util.Response;

@RestController
public class AccountController {
	
	@Autowired
	AccountServices as;
	
	@GetMapping("/user")
	public ResponseEntity<Response> get_all_account(){
		Response response = new Response();
		List<Account> listAcc = as.get_all_account();
		if(listAcc == null) {
			response.setMessage("No Data");
			response.setStatus("Success");
		}else {
			response.setData(listAcc);
			response.setMessage("Data Found");
			response.setStatus("Success");
		}
		
		return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);
	}
	
	@GetMapping("/user/{email}")
	public ResponseEntity<Response> get_account(@PathVariable("email") String email){
		Response response = new Response();
		Account acc = as.get_account(email);
		if(acc == null) {
			response.setMessage("No Data");
			response.setStatus("Success");
		}else {
			response.setData(acc);
			response.setMessage("Data Found");
			response.setStatus("Success");
		}
		return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);
	}
	
	@RequestMapping(value = "/user", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public ResponseEntity<Response> insert_account(@RequestBody Account acc){
		Response response = new Response();
		Integer id = as.insert_account(acc);
		if (id == -1 ) {
			response.setMessage("Failed insert");
			response.setStatus("Failed");
		}else {
			response.setData(acc);
			response.setMessage("Successinsert");
			response.setStatus("Success");
		}
		
		return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);
	}
	
	@DeleteMapping("/user/{email}")
	public ResponseEntity<Response> delete_account(@PathVariable ("email") String email){
		Response response = new Response();
		Account acc = as.get_account(email);
		if(acc != null) {
			as.delete_account(email);
			response.setData(acc);
			response.setMessage("Success Delete");
			response.setStatus("Success");
		}else {
			response.setMessage("Failed Detele");
			response.setStatus("Failed");
		}
		return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);
	}
	
	@PutMapping("/user")
	public ResponseEntity<Response> update_account(@PathVariable ("email") String email){
		Response response = new Response();
		Account acc = as.get_account(email);
		if(acc != null) {
			as.update_account(acc);
			response.setData(acc);
			response.setMessage("Success Update");
			response.setStatus("Success");
		}else {
			response.setMessage("Failed Update");
			response.setStatus("Failed");
		}
		return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);
	}
}
