package com.onlineassement.andreas.OnlineAssesment.model;

import lombok.Data;

@Data
public class Account {
    public Integer user_id;
    public String username;
    public String password;
    public String email;
}